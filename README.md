client-probe
---
A JS utility to probe user client for Operating System and Web browser being used and
represent it via attribute value of `<body>` element and in JS via Boolean flag.

### Why ?

Working on a website/webapp that supports multiple browsers across multiple OSs is a hard problem.
We often end up with bugs that only affect certain browser and OS combination, and fixing
these bugs often involve _hacks_, Eg; using specific CSS selector that is identified only by
certain browsers. For instance, [Browserhacks](http://browserhacks.com/) has a brief list
of useful selector and JS hacks that can be used to target fixes only for certain
browsers & OSs. But such hacks often come with unexpected side-effects and are also not
readable or consistent if browser or OS version is changed.

_Client Probe_ does browser and OS detection using `userAgent` string supplied by the browser
and then adds flags as an attribute value of `<body>` (to use in a selector in CSS) and as a Boolean
flag on provided object to use it in JS.

### Example ?

With default configuration of _client-probe_, opening a webpage in Chrome running under Linux
will lead to `<body>` element having CSS classes present as `platform-linux` and `browser-chrome`,
while the `window` object in JS will have Boolean flags `isPlatformLinux` & `isBrowserChrome` as `true`.
This makes it easy to write predictable fixes which are only applied on target platform and browser,
leaving rest of the platfrom/browser combinations unaffected.

### Installation

client-probe can be used as a plain JS library via `<script>` tag within webpage.

```html
<head>
  <!-- -->
  <script src="client-probe.js"></script>
</head>
```

Alternatively, if you're using package manager to maintain dependencies, you can install client-probe
via `yarn` or `npm` using the command `yarn add client-probe` or `npm install -S client-probe` and then
you can import it in ES6 file via `import`;

```javascript
import clientProbe from 'client-probe';
```

### Usage

Once installed, `clientProbe` object can be initialized as follows;

```javascript
var cp = clientProbe(window, {...});
```

Here, first param is client on which probing is done, usually it is `window` object. Second param is
map of options.

#### Configuration Options

-  `initOnLoad`: Determines whether to initialize flags as soon as library is loaded. Defaults to `true`.
-  `enableCssFlags`: Initialize CSS flags. Defaults to `true`.
-  `enableJsFlags`: Initialize JS flags. Defaults to `true`.
-  `cssFlagOptions`: Options for CSS flags.
    -  `browserFlagPrefix`: Prefix to use for flag representing browser. Defaults to `browser-`, where
      browser name in small letters is suffixed, eg; `browser-chrome`.
    -  `platformFlagPrefix`: Prefix to use for flag representing platform. Defaults to `platform-`, where
      platform name in small letters is suffixed, eg; `platform-linux`.
    -  `flagAttribute`: Attribute of `<body>` element to update with flags. Defaults to `class`, where
      flags are added as value of _class_ attribute of `<body>`.
-  `jsFlagOptions`: Options for JS flags.
    -  `browserFlagPrefix`:  Prefix to use for flag representing browser. Defaults to `isBrowser`, where
      browser name in capitalized letters is suffixed, eg; `isBrowserChrome`.
    -  `platformFlagPrefix`: Prefix to use for flag representing platform. Defaults to `isPlatform`, where
      platform name in capitalized letters is suffixed, eg; `isPlatformLinux`.
    - `flagParent`: Object on which JS flags are initialized. Defaults to _client_ (first param provided to
      `clientProbe` method i.e. `window`).

#### Methods

-  `init`: In case `initOnLoad` was set to `false` in configuration options, this method can be manually called
  on `clientProbe` instance object to trigger probing and initializing flags.
-  `clearFlags`: If flags are causing unexpected conflicts with your webpage, you can revert back the changes
  done by it by calling this method.

### License

MIT

### Author

[Kushal Pandya](https://gitlab.com/kushalpandya)
